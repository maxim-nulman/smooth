<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use Illuminate\Support\Facades\Validator;

/**
 * 
 */
class ProductController extends ApiController
{    
    
    /**
     * List all products
     * 
     * @param Request $request
     * @return type
     */
    public function index(Request $request)
    {
        return Product::all()->toArray();
    }
    
    /**
     * get one by ID
     * 
     * @param Request $request
     * @return type
     */
    public function one(Request $request)
    {
        return Product::find($request->id);
    }
    
    /**
     * delete one by ID
     * 
     * @param Request $request
     * @return type
     */
    public function delete(Request $request)
    {
        return ['delete' => Product::find($request->id)->delete()];
    }    
    
    /**
     * Show orders.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'category_id' => 'required|integer',
            'price' => 'required|numeric',
            'sku' => 'required|max:255',
        ]);
        
        if ($validator->fails()) {
            
            return $this->error(500, $validator->errors());
            
        } 
        
        if (empty($game)) {
        
            $item = new Product;

            $item->name = $request->input('name');

            $item->category_id = $request->input('category_id');

            $item->price = $request->input('price');
            
            $item->sku = $request->input('sku');

            try {
            
                $item->save();
            
            } catch (\Exception $e) {
                
                return $this->error(500, "Error: failed to create a product");
                
            }
        
        }
        
        return ['id' => $item->id];
        
    }
    
}
