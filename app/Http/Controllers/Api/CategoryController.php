<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Category;

/**
 * 
 */
class CategoryController extends ApiController
{    
    
    /**
     * List all products
     * 
     * @param Request $request
     * @return type
     */
    public function index(Request $request)
    {
        return Category::all()->toArray();
    }
       
}
