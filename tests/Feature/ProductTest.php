<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase {

    /**
     * Test validation
     *
     * @return void
     */
    public function testCreateFail() {

        $this->json('POST', 'api/product/create', ['Accept' => 'application/json'])
        ->assertStatus(500)
        ->assertJson([
            "error" => [
                "name" =>  [
                    "The name field is required."
                ],
                "category_id" => [
                    "The category id field is required."
                ],
                "price" => [
                    "The price field is required."
                ],
                "sku" => [
                    "The sku field is required."
                ]
            ]
        ]);
    }

}
