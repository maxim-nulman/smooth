<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('product/list', 'Api\ProductController@index');
Route::get('product/{id}', 'Api\ProductController@one');
Route::post('product/create', 'Api\ProductController@create');
Route::delete('product/delete/{id}', 'Api\ProductController@delete');
Route::get('category/list', 'Api\CategoryController@index');