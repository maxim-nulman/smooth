<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'id' => $faker->id,
        'name' => $faker->name,
        'category_id' => $faker->category_id,
        'sku' => $faker->sku,
        'price' => $faker->price,
        'created_at' => $faker->created_at,
        'updated_at' => $faker->updated_at,
    ];
});